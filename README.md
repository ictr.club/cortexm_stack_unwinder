# cortexm_stack_unwinder

Calisma zamaninda hata atan Cortex M islemcilerin geriye donuk hangi fonksiyonlarin cagrildigini inceleme amaciyla hazirlanmistir.
THUMB2 mod denilen makine kodu icin hazirlanmistir. ARM mod icin uygun degildir. 
Baz alinan dokumanlar
[Exception Handling ABI for the Arm®
Architecture
2020Q4](https://github.com/ARM-software/abi-aa/releases)


====================================================================================================

This library is prepared for run-time debugging of Cortex-M processors. It is aimed for Cortex M3/M4 (without VFP). Since THUMB2 frame pointer mechanism is much more different than ARM mode, callstack generation can't be done by using FP itself. Hence, unwind-tables should be used instead of only frame pointer based one.

Documents
[Exception Handling ABI for the Arm®
Architecture
2020Q4](https://github.com/ARM-software/abi-aa/releases)
