/*
 * cortex_m3_stack_unwinder.c
 *
 *  Created on: Jan 1, 2021
 *      Author: can
 */

#include <stdint.h>
#include "cortex_m_stack_unwinder.h"

extern uint32_t __exidx_end, __exidx_start;

#define CHECK_BIT32(x,y)     ((x&(1UL<<(y)))?1:0)

#ifndef TRUE
	#define TRUE 1ULL
#endif

#ifndef FALSE
	#define FALSE 0ULL
#endif

#ifndef EMPTY
	#define EMPTY -1LL
#endif

#ifndef NULL
	#define NULL 0ULL
#endif


/*we will use exidx start and end addresses*/
/*Find table index from instruction pointer*/
static const struct unwind_table_index_s* find_uti_from_ip(uint32_t pc)
{
	const struct unwind_table_index_s* begin, *end, *mid;
	uint32_t total_entry_count, i, func_addr;


	begin = (const struct unwind_table_index_s*)&__exidx_start;
	end = (const struct unwind_table_index_s*)&__exidx_end;

	total_entry_count = (uint32_t)(end - begin);

	/*Binary search will be used*/
	for (i = 0; i < total_entry_count; i++) {
		mid = &begin[(i + total_entry_count) >> 1];
		func_addr = PREL31_CONVERT(&mid -> uti_addr_offset);

		/*
		 * Mid point is far away from our inst. pointer
		 * No need to search mid to end, it is not there
		 */
		if (pc < func_addr) {
			total_entry_count = (i + total_entry_count) >> 1;
			i = -1;
		} else {
			/*
			 * Mid point is lower our inst. pointer
			 * No need to search begin to mid, it is not there
			 */
			i = (i + total_entry_count) >> 1;
		}
	}

	if (func_addr <= pc )
		return mid;
	else
		return NULL;
}

/*
 * Assembly format for -mpoke-function-name switch
 * t0
 *	.ascii "function", 0
 *	.align
 * t1
 *	.word 0xFF000000 + (t1 - t0)
 * function
 *	mov Rx, Rx
 *	sub Rx, Ry
 *	.....
 *	We have function start address,
 *	all we need to do is to go back 4 bytes read magic word
 * */
static const char* get_func_name(uint32_t func_start_addr)
{
	uint32_t *magic_number_ptr = (uint32_t*)(func_start_addr
												- sizeof(uint32_t));
	const char *func_name_ptr;
	uint32_t offset;


	if ((*magic_number_ptr & 0xFF000000) == 0xFF000000) {
		offset = func_start_addr - sizeof(uint32_t);
		offset -= (*magic_number_ptr & 0x00FFFFFF);
		func_name_ptr = (const char*)offset;
		return func_name_ptr;
	} else {
		return NULL;
	}
}

static uint8_t get_next_ins_byte(const uint32_t **ins_table_ptr_ptr,
									uint32_t *byte_ptr,
									uint32_t *entry_cnt_ptr)
{
	uint8_t ins_byte;


	/*Don't do anything, stack is corrupted :( */
	if (*entry_cnt_ptr <= 0)
		return 0;

	ins_byte = (**ins_table_ptr_ptr >> (*byte_ptr << 3)) & 0xFF;

	if (*byte_ptr == 0) {
		(*ins_table_ptr_ptr)++;
		(*entry_cnt_ptr)--;
		*byte_ptr = 3;
	} else {
		(*byte_ptr)--;
	}

	return ins_byte;
}

static uint8_t execute_instructions(struct cortexm_stack_frame_s *csf_ptr,
									const uint32_t *ins_table_ptr,
									uint32_t byte,
									uint32_t entry_count)
{
	uint8_t ins_byte, aux_ins_byte, i;
	uint32_t *sp_ptr;
	uint16_t r4_r15_reg_winder;


	while (entry_count > 0) {
		ins_byte = get_next_ins_byte(&ins_table_ptr, &byte, &entry_count);

		/*ARM EHABI section 10.3*/
		if ((ins_byte & 0xC0) == 0x00) {
			csf_ptr -> csf_reg_set[CRS_SP] += ((ins_byte & 0x3F) << 2) + 4;
		} else 	if ((ins_byte & 0xC0) == 0x40) {
			csf_ptr -> csf_reg_set[CRS_SP] -= ((ins_byte & 0x3F) << 2) - 4;
		} else if ((ins_byte & 0xF0) == 0x80) { /*Pop-up 12 (r4-r15) registers*/
			aux_ins_byte = get_next_ins_byte(&ins_table_ptr, &byte,
					&entry_count);

			r4_r15_reg_winder = ins_byte;
			r4_r15_reg_winder <<= 8;
			r4_r15_reg_winder |= aux_ins_byte;
			/*Refuse to unwind*/
			if (r4_r15_reg_winder == 0x8000)
				return FALSE;

			r4_r15_reg_winder &= 0x0FFF;
			sp_ptr = (uint32_t*)csf_ptr -> csf_reg_set[CRS_SP];

			for (i = 0; i <= (CRS_PC - CRS_R4); i++) {
				if (r4_r15_reg_winder & (1 << i)) {
					csf_ptr -> csf_reg_set[i + CRS_R4] = *sp_ptr++;
				}
			}

			if (!CHECK_BIT32(r4_r15_reg_winder, CRS_SP - CRS_R4))
				csf_ptr -> csf_reg_set[CRS_SP] = (uint32_t)sp_ptr;
		} else if ((ins_byte & 0xF0) == 0x90) { /*Set SP to another reg*/
			ins_byte &= 0x0F;
			if (ins_byte != CRS_SP && ins_byte != CRS_PC)
				csf_ptr -> csf_reg_set[CRS_SP]
											= csf_ptr -> csf_reg_set[ins_byte];
		} else if ((ins_byte & 0xF0) == 0xA0) { /*Pop-up (r4-r[4+n]),r14 registers*/
			r4_r15_reg_winder = ins_byte & 0x07;
			sp_ptr = (uint32_t*)csf_ptr -> csf_reg_set[CRS_SP];

			for (i = 0; i <= r4_r15_reg_winder; i++) {
				csf_ptr -> csf_reg_set[i + CRS_R4] = *sp_ptr++;
			}

			if (ins_byte & 0x08)
				csf_ptr -> csf_reg_set[CRS_LR] = *sp_ptr++;

			csf_ptr -> csf_reg_set[CRS_SP] = (uint32_t)sp_ptr;
		} else if (ins_byte == 0xB0) { /*Finish what we started*/
			if (csf_ptr -> csf_reg_set[CRS_PC] == 0x00)
				csf_ptr -> csf_reg_set[CRS_PC] = csf_ptr -> csf_reg_set[CRS_LR];

			return TRUE;
		} else if (ins_byte == 0xB1) { /*Pop r0-r4 registers*/
			aux_ins_byte = get_next_ins_byte(&ins_table_ptr, &byte,
															&entry_count);
			if (aux_ins_byte == 0x00)
				return FALSE;

			sp_ptr = (uint32_t*)csf_ptr -> csf_reg_set[CRS_SP];
			for (i = 0; i < CRS_R4; i++) {
				if (aux_ins_byte & (1 << i)) {
					csf_ptr -> csf_reg_set[i] = *sp_ptr++;
				}
			}

			csf_ptr -> csf_reg_set[CRS_SP] = (uint32_t)sp_ptr;
		} else if (ins_byte == 0xB2) { /*Increase Stack pointer*/
			aux_ins_byte = get_next_ins_byte(&ins_table_ptr, &byte,
															&entry_count);

			csf_ptr -> csf_reg_set[CRS_SP] += (0x204
											+ (((uint32_t)aux_ins_byte) << 2));
		} else if (ins_byte == 0xFF) {
			return FALSE;
		}
	}
	/*TODO: Handle Floating point registers(VFP) for M4/M7*/
	return FALSE;
}

/*Unwind a frame with given information*/
static uint8_t unwind_frame(struct cortexm_stack_frame_s *csf_ptr,
							const struct unwind_table_index_s *uti_ptr)
{
	const uint32_t *instruction_table_ptr;
	const uint32_t *ins_ptr;
	uint32_t byte, entry_count;


	ins_ptr = (const uint32_t*)&uti_ptr -> uti_inst;
	/*Unwindable frame*/
	if (*ins_ptr == 1)
		return FALSE;

	/*EHABI doc. sec 7.2 generic entry*/
	if ((*ins_ptr & 0x80000000) == 0) {
		instruction_table_ptr = (uint32_t*)PREL31_CONVERT(ins_ptr);
	} else if ((*ins_ptr & 0xFF000000) == 0x80000000) {
		instruction_table_ptr = ins_ptr;
	} else {
		return FALSE;
	}

	if ((*instruction_table_ptr & 0xFF000000) == 0x80000000) {
		byte = 2;
		entry_count = 1;
	} else 	if ((*instruction_table_ptr & 0xFF000000) == 0x81000000) {
		byte = 1;
		entry_count = 1 + ((*instruction_table_ptr & 0x00FF0000) >> 16);
	}


	if (execute_instructions(csf_ptr, instruction_table_ptr,
											byte, entry_count) == FALSE) {
		return FALSE;
	}
	return TRUE;
}

static uint32_t unwind_stacktrace(struct cortexm_stack_frame_s *frame_buff_ptr,
									uint32_t frame_size)
{
	uint32_t i = 0;
	const struct unwind_table_index_s *uti_ptr;


	for (i = 0; i < (frame_size - 1); i++) {
		uti_ptr = find_uti_from_ip(frame_buff_ptr[i].csf_reg_set[CRS_PC]);

		if (uti_ptr == (typeof(uti_ptr))NULL)
			goto out;

		frame_buff_ptr[i].csf_func_name = get_func_name(
								PREL31_CONVERT(&uti_ptr -> uti_addr_offset));

		frame_buff_ptr[i + 1].csf_reg_set[CRS_PC] = 0;
		frame_buff_ptr[i + 1].csf_reg_set[CRS_FP] =
							frame_buff_ptr[i].csf_reg_set[CRS_FP];
		frame_buff_ptr[i + 1].csf_reg_set[CRS_SP] =
							frame_buff_ptr[i].csf_reg_set[CRS_SP];
		frame_buff_ptr[i + 1].csf_reg_set[CRS_LR]
										= frame_buff_ptr[i].csf_reg_set[CRS_LR];
		if (unwind_frame(&frame_buff_ptr[i + 1], uti_ptr) == FALSE)
			goto out;


	}
	out:
		return i;
}


void __unhandled_exception(struct cortex_m3_exception_frame_s *cmef_ptr,
		uint32_t top_fp)
{
	struct cortexm_stack_frame_s frame_buff[20];
	uint32_t num_of_resolved_frame;


	for (int i = 0; i < sizeof(frame_buff); i++) {
		((uint8_t*)frame_buff)[i] = 0x00;
	}
	frame_buff[0].csf_reg_set[CRS_SP] = ((uint32_t)cmef_ptr)
								+ sizeof(struct cortex_m3_exception_frame_s);
	frame_buff[0].csf_reg_set[CRS_LR] = cmef_ptr -> cmef_lr;
	frame_buff[0].csf_reg_set[CRS_PC] = cmef_ptr -> cmef_pc;
	frame_buff[0].csf_reg_set[CRS_FP] = top_fp;

	num_of_resolved_frame = unwind_stacktrace(frame_buff, 20);

	/*Now we have all caller function addresses and names*/
	while(1);
}

/*
 * Cortex M3/M4 exception stack without Floating Point Unit
 * ============
 * |prev. SP  |
 * ============
 * |   xPSR   |
 * ============
 * |    PC    |
 * ============
 * |    LR    |
 * ============
 * |   R12    |
 * ============
 * |    R3    |
 * ============
 * |    R2    |
 * ============
 * |    R1    |
 * ============
 * |    R0    |
 * ============
 */
__attribute__((naked)) void HardFault_Handler()
{
	asm("TST    LR, #4");//
	asm("ITE EQ");
	asm("MRSEQ R0, MSP");
	asm("MRSNE R0, PSP");
	asm("MOVS R1, R7");
	asm("BL __unhandled_exception");
}
