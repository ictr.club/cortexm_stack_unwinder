/*
 * cortex_m_stack_unwinder.h
 *
 *  Created on: Jan 3, 2021
 *      Author: can
 */

#ifndef COMMON_ARCH_CORTEX_M_STACK_UNWINDER_H_
#define COMMON_ARCH_CORTEX_M_STACK_UNWINDER_H_

#include <stdint.h>

/*It converts given pointed number to signed number as offset of address */
#define PREL31_CONVERT(ptr)	(((((int32_t)*ptr) << 1) >> 1) + (uint32_t)ptr)

/*Thumb 2 mode register set*/
enum cortexm_reg_set_e
{
	CRS_R4 = 4,
	CRS_FP = 7,
	CRS_SP = 13,
	CRS_LR = 14,
	CRS_PC = 15,
	NUM_OF_CORTEXM_REGISTERS
};

struct cortexm_stack_frame_s
{
	uint32_t csf_reg_set[NUM_OF_CORTEXM_REGISTERS];
	const char *csf_func_name;
};

struct unwind_table_index_s
{
	uint32_t uti_addr_offset;
	uint32_t uti_inst;
};

struct cortex_m3_exception_frame_s
{
	uint32_t	cmef_r0,
				cmef_r1,
				cmef_r2,
				cmef_r3,
				cmef_r12,
				cmef_lr,
				cmef_pc,
				cmef_xpsr;
};



#endif /* COMMON_ARCH_CORTEX_M_STACK_UNWINDER_H_ */
